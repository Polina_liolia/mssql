﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoredProcSquareEqualty_
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security=True";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmdStoredProc = new SqlCommand();
                SqlParameter inParam1 = new SqlParameter("@a", System.Data.SqlDbType.Int);
                SqlParameter inParam2 = new SqlParameter("@b", System.Data.SqlDbType.Int);
                SqlParameter inParam3 = new SqlParameter("@c", System.Data.SqlDbType.Int);
                inParam1.Value = "1";
                inParam2.Value = "0";
                inParam3.Value = "-9";

                SqlParameter outvalOUT_PARAM1 = new SqlParameter("@out_RESUN", System.Data.SqlDbType.VarChar, 200);
                SqlParameter outvalOUT_PARAM2 = new SqlParameter("@x1", System.Data.SqlDbType.Float);
                SqlParameter outvalOUT_PARAM3 = new SqlParameter("@x2", System.Data.SqlDbType.Float);
                outvalOUT_PARAM1.Direction = System.Data.ParameterDirection.Output;
                outvalOUT_PARAM2.Direction = System.Data.ParameterDirection.Output;
                outvalOUT_PARAM3.Direction = System.Data.ParameterDirection.Output;
                cmdStoredProc.Parameters.Add(outvalOUT_PARAM1);
                cmdStoredProc.Parameters.Add(outvalOUT_PARAM2);
                cmdStoredProc.Parameters.Add(outvalOUT_PARAM3);

                cmdStoredProc.Parameters.Add(inParam1);
                cmdStoredProc.Parameters.Add(inParam2);
                cmdStoredProc.Parameters.Add(inParam3);
                cmdStoredProc.CommandText = "CALC_SOLUTION_X1X2";
                cmdStoredProc.CommandType = System.Data.CommandType.StoredProcedure;

                cmdStoredProc.Connection = conn;
                conn.Open();
                cmdStoredProc.ExecuteNonQuery();
                Console.WriteLine("out value is {0}", outvalOUT_PARAM1.Value);
                Console.WriteLine("x1 is {0}", outvalOUT_PARAM2.Value);
                Console.WriteLine("x2 is {0}", outvalOUT_PARAM3.Value);
                conn.Close();
            }
        }
    }
}
