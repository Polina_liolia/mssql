use AdventureWorks2008;
--Programmability -> Database Triggers
--go
--CREATE TRIGGER safety 
--ON DATABASE 
--FOR DROP_TABLE, ALTER_TABLE 
--AS 
--   PRINT 'You must disable Trigger "safety" to drop or alter tables!' 
--   ROLLBACK;

--test:
drop table [HumanResources].[Employee] --�� ����������


--This is the database table
IF OBJECT_ID (N'dbo.SampleTable', N'U') IS NOT NULL  
DROP TABLE [dbo].[SampleTable] --�� ����������

CREATE TABLE dbo.SampleTable
	(
	Id int NOT NULL IDENTITY (1, 1),
	SampleString nvarchar(64) NOT NULL,
	SampleInt int NULL
	)  ON [PRIMARY]
go
SELECT OBJECT_ID(N'dbo.SampleTable') AS 'Object ID'
