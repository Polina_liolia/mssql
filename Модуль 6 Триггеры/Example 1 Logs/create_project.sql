USE [SampleDb]
GO

IF OBJECT_ID (N'dbo.Project', N'U') IS NOT NULL  
DROP TABLE [dbo].[Project]
GO

/****** Object:  Table [dbo].[Project]    Script Date: 27.02.2017 23:44:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Project](
	[Number] [char](4) NOT NULL,
	[ProjectName] [nchar](15) NOT NULL,
	[Budget] [float] NULL,
 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


