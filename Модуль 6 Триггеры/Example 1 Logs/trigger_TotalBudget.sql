USE [AdventureWorks2008]
GO
CREATE TRIGGER trigger_TotalBudget
    ON Project AFTER UPDATE
    AS IF UPDATE (Budget)
BEGIN
    DECLARE @sum_old1
    FLOAT DECLARE @sum_old2
    FLOAT DECLARE @sum_new FLOAT

    SELECT @sum_new = (SELECT SUM(Budget) FROM inserted)
    SELECT @sum_old1 = (SELECT SUM(p.Budget)
        FROM project p WHERE p.Number
            NOT IN (SELECT d.Number FROM deleted d))
    SELECT @sum_old2 = (SELECT SUM(Budget) FROM deleted)
    
    IF @sum_new > (@sum_old1 + @sum_old2) * 1.5
    BEGIN
        PRINT N'������ �� ���������'
        ROLLBACK TRANSACTION
    END
    ELSE
        PRINT N'��������� ������� ���������'
END