﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions_and_SP_call
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security=True";
            //вызов скалярной функции 
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmdStoredFunction = new SqlCommand();
                SqlParameter inParam1 = new SqlParameter("@p1", System.Data.SqlDbType.Int);
                SqlParameter inParam2 = new SqlParameter("@p2", System.Data.SqlDbType.Int);
                inParam1.Value = "2";
                inParam2.Value = "7";
                /* SqlParameter outvalOUT_PARAM1 = new SqlParameter("@out_sum", System.Data.SqlDbType.VarChar, 200); 
                 * outvalOUT_PARAM1.Direction = System.Data.ParameterDirection.Output; 
                 * cmdStoredFunction.Parameters.Add(outvalOUT_PARAM1); */
                cmdStoredFunction.Parameters.Add(inParam1);
                cmdStoredFunction.Parameters.Add(inParam2);
                cmdStoredFunction.CommandText = "select dbo.udf_Product(@p1,@p2) as Product";
                cmdStoredFunction.CommandType = System.Data.CommandType.Text;
                cmdStoredFunction.Connection = conn;
                conn.Open();
                SqlDataReader reader_ = cmdStoredFunction.ExecuteReader();
                while (reader_.Read())
                {
                    int v1 = reader_.GetInt32(0); Console.WriteLine("Product is {0}", v1);
                }
                reader_.Close();
                conn.Close();
            }
            //вызов inline табличной функции 
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmdStoredFunction = new SqlCommand();
                SqlParameter inParam1 = new SqlParameter("@p1", System.Data.SqlDbType.Int);
                inParam1.Value = "602";
                cmdStoredFunction.Parameters.Add(inParam1);
                cmdStoredFunction.CommandText = "SELECT * FROM Sales.ufn_SalesByStore (@p1)";
                cmdStoredFunction.CommandType = System.Data.CommandType.Text;
                cmdStoredFunction.Connection = conn;
                conn.Open();
                SqlDataReader reader = cmdStoredFunction.ExecuteReader();
                while (reader.Read())
                {
                    int v1 = reader.GetInt32(1);
                    string v2 = reader.GetString(2);
                    decimal v3 = reader.GetDecimal(3);
                    Console.WriteLine("{0} {1} {2}", v1, v2, v3);
                }
                reader.Close();
                conn.Close();
            }
        }
    }
}
