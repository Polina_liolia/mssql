use [AdventureWorks2008];

GO
SELECT OBJECT_ID(N'dbo.SampleTable') AS 'Object ID'
GO
IF OBJECT_ID (N'dbo.SampleTable', N'U') IS NOT NULL
DROP TABLE [dbo].[SampleTable]
GO
--This is the database table
CREATE TABLE dbo.SampleTable
(
Id int NOT NULL IDENTITY (1, 1),
SampleString nvarchar(64) NOT NULL,
SampleInt int NULL
) ON [PRIMARY]
go

--creating table type:
CREATE TYPE [dbo].[SampleDataType] As Table
(
--This type has structure similar to the DB table
SampleString Nvarchar(64) Not Null, -- Having one String
SampleInt Int -- and one int
)
go
--This is the Stored Procdeure
CREATE PROCEDURE [dbo].[SampleProcedure]
(
-- which accepts one table value parameter. It should be noted that the parameter is readonly
@Sample As [dbo].[SampleDataType] Readonly
)
AS
Begin
-- We simply insert values into the DB table from the parameter
-- The table value parameter can be used like a table with only read rights
Insert Into SampleTable(SampleString,SampleInt)
Select SampleString, SampleInt From @Sample
End


go
-- An instance of the Table parameter type is created
Declare @SampelData As [dbo].[SampleDataType]
-- and then filled with the set of values
Insert Into @SampelData(SampleString, SampleInt) Values('1',1);
Insert Into @SampelData(SampleString, SampleInt) Values('2',null);
Insert Into @SampelData(SampleString, SampleInt) Values('3',3);

Select * From @SampelData

-- then we call the SP to store the values
Exec SampleProcedure @SampelData

--results of SP execution:
Select * From SampleTable