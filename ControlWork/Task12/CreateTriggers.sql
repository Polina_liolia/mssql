use AdventureWorks2008;

GO
IF EXISTS (SELECT * FROM sys.triggers WHERE [name] = 'my_safety_create_log_trigger')
BEGIN
	DROP TRIGGER [my_safety_create_log_trigger] ON DATABASE;
	PRINT 'TRIGGER my_safety_create_log_trigger DROPED';
END
GO
CREATE TRIGGER my_safety_create_log_trigger
ON DATABASE 
FOR CREATE_TABLE
AS 
	BEGIN
		PRINT N'Выполнена операция CREATE_TABLE.'; 
	END


GO
IF EXISTS (SELECT * FROM sys.triggers WHERE [name] = 'my_safety_drop_log_trigger')
BEGIN
	DROP TRIGGER [my_safety_drop_log_trigger] ON DATABASE;
	PRINT 'TRIGGER my_safety_drop_log_trigger DROPED';
END
GO
CREATE TRIGGER my_safety_drop_log_trigger
ON DATABASE 
FOR DROP_TABLE
AS 
	BEGIN
		PRINT N'Выполнена операция DROP_TABLE.'; 
	END

GO
IF EXISTS (SELECT * FROM sys.triggers WHERE [name] = 'my_safety_alter_log_trigger')
BEGIN
	DROP TRIGGER [my_safety_alter_log_trigger] ON DATABASE;
	PRINT 'TRIGGER my_safety_alter_log_trigger DROPED';
END
GO
CREATE TRIGGER my_safety_alter_log_trigger
ON DATABASE 
FOR ALTER_TABLE
AS
	BEGIN 
		PRINT N'Выполнена операция ALTER_TABLE.';
	END