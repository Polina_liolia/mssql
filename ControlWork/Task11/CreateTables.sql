USE AdventureWorks2008;

GO 
IF EXISTS (SELECT * FROM sys.tables WHERE [name] = 'MYLOGS') 
DROP TABLE [DBO].[MYLOGS];
CREATE TABLE MYLOGS(
MYLOGS_ID INT IDENTITY(1,1) NOT NULL,
ACTION NCHAR(50) NOT NULL,
TBL_NAME VARCHAR(50) NOT NULL,
ID_ROW INT NOT NULL,
LOG_INFO NVARCHAR(100) NULL,
LOG_TIME DATETIME NOT NULL DEFAULT GETDATE()
);
 

GO 
IF EXISTS (SELECT * FROM sys.tables WHERE [name] = 'USERS_TO_ROLES') 
DROP TABLE [DBO].[USERS_TO_ROLES];
CREATE TABLE USERS_TO_ROLES(
ID INT IDENTITY(1,1) NOT NULL,
USER_ID INT NOT NULL,
ROLE_ID INT NOT NULL,
);

GO
IF EXISTS (SELECT * FROM sys.tables WHERE [name] = 'USERS') 
DROP TABLE [DBO].[USERS];
CREATE TABLE USERS(
ID INT IDENTITY(1,1) NOT NULL,
USER_NAME VARCHAR(50) NOT NULL
);

GO
IF EXISTS (SELECT * FROM sys.tables WHERE [name] = 'ROLES') 
DROP TABLE [DBO].[ROLES];
CREATE TABLE ROLES(
ID INT IDENTITY(1,1) NOT NULL,
ROLE_NAME VARCHAR(30) NOT NULL,
[RULE] NCHAR(30) NOT NULL
);


ALTER TABLE [dbo].[USERS] ADD CONSTRAINT PK_USERS PRIMARY KEY (ID);
ALTER TABLE [dbo].[ROLES] ADD CONSTRAINT PK_ROLES PRIMARY KEY (ID);
ALTER TABLE [dbo].[USERS_TO_ROLES] ADD CONSTRAINT PK_USERS_TO_ROLES PRIMARY KEY (ID);
ALTER TABLE [dbo].[USERS_TO_ROLES] ADD CONSTRAINT FK_USERS_TO_ROLES_USER_ID FOREIGN KEY (USER_ID) REFERENCES [DBO].[USERS](ID);
ALTER TABLE [dbo].[USERS_TO_ROLES] ADD CONSTRAINT FK_USERS_TO_ROLES_ROLE_ID FOREIGN KEY (ROLE_ID) REFERENCES [DBO].[ROLES](ID);
ALTER TABLE [dbo].[MYLOGS] ADD CONSTRAINT PK_MYLOGS PRIMARY KEY (MYLOGS_ID);