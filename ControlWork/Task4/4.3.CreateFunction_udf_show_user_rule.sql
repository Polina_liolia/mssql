USE AdventureWorks2008;
GO

IF OBJECT_ID (N'udf_show_user_rule', N'IF') IS NOT NULL  
    DROP FUNCTION [udf_show_user_rule];  
GO  
CREATE FUNCTION udf_show_user_rule(@id int)
RETURNS TABLE AS
	RETURN 
	(
		SELECT 
		ROLES.[RULE] 
		FROM [dbo].[USERS_TO_ROLES] UTR
		LEFT JOIN [dbo].[ROLES] ROLES ON UTR.ROLE_ID = ROLES.ID
		WHERE UTR.USER_ID = @id
	);

--TEST FUNCTION udf_show_user_rule
GO
USE AdventureWorks2008;
select * FROM [dbo].[udf_show_user_rule](1) as UsersRules;

