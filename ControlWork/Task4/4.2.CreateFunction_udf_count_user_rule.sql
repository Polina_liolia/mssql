USE AdventureWorks2008;
go
IF OBJECT_ID (N'udf_count_user_rule', N'FN') IS NOT NULL  
    DROP FUNCTION [udf_count_user_rule];  
GO  
CREATE FUNCTION udf_count_user_rule(@rule nchar(30))
RETURNS INT AS
BEGIN
	RETURN (
		SELECT 
		COUNT(UTR.ID) 
		FROM [dbo].[USERS_TO_ROLES] UTR
		LEFT JOIN [dbo].[ROLES] ROLES ON UTR.ROLE_ID = ROLES.ID
		WHERE ROLES.[RULE] = @rule
	);
END;

--TEST FUNCTION udf_count_user_rule
GO
USE AdventureWorks2008;
select [dbo].[udf_count_user_rule]('U') as UsersAbleToUpdate;




