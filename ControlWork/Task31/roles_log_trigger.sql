USE AdventureWorks2008;
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE [name] = 'roles_log_trigger')
	DROP TRIGGER [roles_log_trigger]
GO
CREATE TRIGGER roles_log_trigger
ON [dbo].[ROLES]
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;
	--defining type of action (insert / update / delete):
    DECLARE @action as char(1);
    IF EXISTS(SELECT * FROM DELETED)
		BEGIN
			SET @action = 
				CASE
					WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' -- Set Action to Updated (data found in both DELETED and INSERTED)
					ELSE 'D' -- Set Action to Deleted (data found in DELETED only)       
				END
		END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
		RETURN; -- Nothing updated or inserted.
	ELSE 
		SET @action = 'I'; -- Set Action to Inserted (data found in INSERTED only) 

	--switching actions:
	DECLARE @LOG_INFO NVARCHAR(100);
	SET @LOG_INFO = 
		CASE @action
			when 'U' 
				then N'����: ID=' + CAST((SELECT [ID] FROM DELETED) AS NVARCHAR(4)) + ', ROLE_NAME=' + 
				(SELECT [ROLE_NAME] FROM DELETED) + ', RULE=' + CAST((SELECT [RULE] FROM DELETED) AS NVARCHAR(30)) +
				 N'; �����: ID=' + CAST((SELECT [ID] FROM INSERTED) AS NVARCHAR(4)) + ', ROLE_NAME=' + 
				(SELECT [ROLE_NAME] FROM INSERTED) + ', RULE=' + CAST((SELECT [RULE] FROM INSERTED) AS NVARCHAR(30))
			when 'I' 
				then 'New entry: ID=' + CAST((SELECT [ID] FROM INSERTED) AS NVARCHAR(4)) + ', ROLE_NAME=' + 
				(SELECT [ROLE_NAME] FROM INSERTED) + ', RULE=' + CAST((SELECT [RULE] FROM INSERTED) AS NVARCHAR(30))
			when 'D' 
				then 'Deleted entry: ID=' + CAST((SELECT [ID] FROM DELETED) AS NVARCHAR(4)) + ', ROLE_NAME=' + 
				(SELECT [ROLE_NAME] FROM DELETED) + ', RULE=' + CAST((SELECT [RULE] FROM DELETED) AS NVARCHAR(30))
			else 'UNKNOWN ACTION'
		END

	--defining current row identity:
	DECLARE @ID_ROW INT;
	SET @ID_ROW = 
		CASE @action
				when 'U' 
					then (SELECT [ID] FROM DELETED)
				when 'I' 
					then (SELECT [ID] FROM INSERTED)
				when 'D' 
					then (SELECT [ID] FROM DELETED)
				else -1
			END

	--inserting new lon entry:
	INSERT INTO [dbo].[MYLOGS] ([ACTION], [TBL_NAME], [ID_ROW], [LOG_INFO]) 
	VALUES (@action, 'ROLES', @ID_ROW, @LOG_INFO); 
END

--test trigger:
INSERT INTO [dbo].[ROLES] ([ROLE_NAME], [RULE]) VALUES ('ROLE TEST INSERT TRIGGER', 'SOME RULE');

UPDATE [dbo].[ROLES] 
SET [ROLE_NAME] = 'ROLE TEST UPDATE TRIGGER'
WHERE [ROLE_NAME] = 'ROLE TEST INSERT TRIGGER';

DELETE FROM [dbo].[ROLES] 
WHERE [ROLE_NAME] = 'ROLE TEST UPDATE TRIGGER';

SELECT * FROM [dbo].[MYLOGS]