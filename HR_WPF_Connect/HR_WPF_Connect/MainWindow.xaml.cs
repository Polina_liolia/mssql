﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;

namespace HR_WPF_Connect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataView employeeView;
        public DataView EmployeeView
        {
            get
            {
                return this.employeeView;
            }

            set
            {
                this.employeeView = value;
            }
        }
        
        public MainWindow()
        {
            InitializeComponent();
            EmployeeView = GetHRDataFromDataBase();
            this.gv_person.DataContext = EmployeeView;
        }

        private DataView GetHRDataFromDataBase()
        {
            DataSet data = new DataSet();
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["MSSQL_home"].ConnectionString;
                SqlConnection connection =
                    new SqlConnection(connectionString);
                connection.Open();

                // Create a DataSet.
                data.Locale = System.Globalization.CultureInfo.InvariantCulture;

                // Add data from the Person table to the DataSet.
                SqlDataAdapter masterDataAdapter = new
                    SqlDataAdapter("select top 100 * from [HR].[EMPLOYEES]", connection);
                masterDataAdapter.Fill(data, "Employees");
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system. : " + ex.ToString());
            }
            return data.Tables["Employees"].DefaultView;
        }
    }
}
