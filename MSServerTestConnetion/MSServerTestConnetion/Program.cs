﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace MSServerTestConnetion
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            //string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security=True";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmdStoredProc = new SqlCommand();
                SqlParameter inParam1 = new SqlParameter("@p1", System.Data.SqlDbType.Int);
                SqlParameter inParam2 = new SqlParameter("@p2", System.Data.SqlDbType.Int);
                inParam1.Value = "1";
                inParam2.Value = "2";

                SqlParameter outvalOUT_PARAM1 = new SqlParameter("@out_sum", System.Data.SqlDbType.VarChar, 200);
                outvalOUT_PARAM1.Direction = System.Data.ParameterDirection.Output;
                cmdStoredProc.Parameters.Add(outvalOUT_PARAM1);

                cmdStoredProc.Parameters.Add(inParam1);
                cmdStoredProc.Parameters.Add(inParam2);
                cmdStoredProc.CommandText = "PROC_FOR_TEST";
                cmdStoredProc.CommandType = System.Data.CommandType.StoredProcedure;

                cmdStoredProc.Connection = conn;
                conn.Open();
                cmdStoredProc.ExecuteNonQuery();
                Console.WriteLine("out value is {0}", outvalOUT_PARAM1.Value);
                conn.Close();
            }

            //using (SqlConnection conn = new SqlConnection(connectionString))
            //{
            //    SqlCommand InsertCurrencyCommand = new SqlCommand();
            //    SqlParameter currencyCodeParam = new SqlParameter("@CurrencyCode", System.Data.SqlDbType.NVarChar);
            //    SqlParameter nameParam = new SqlParameter("@Name", System.Data.SqlDbType.NVarChar);
            //    currencyCodeParam.Value = currencyCode;
            //    nameParam.Value = name;
            //    InsertCurrencyCommand.Parameters.Add(currencyCodeParam);
            //    InsertCurrencyCommand.Parameters.Add(nameParam);
            //    InsertCurrencyCommand.CommandText =
            //        "INSERT Sales.Currency (CurrencyCode, Name, ModifiedDate)" +
            //        " VALUES(@CurrencyCode, @Name, GetDate())";
            //    InsertCurrencyCommand.Connection = conn;
            //    conn.Open();
            //    InsertCurrencyCommand.ExecuteNonQuery();
            //    conn.Close();
            //}

        }
    }
}
