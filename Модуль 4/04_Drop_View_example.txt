USE AdventureWorks2008 ;  
GO  
IF OBJECT_ID ('hiredate_view', 'V') IS NOT NULL  
DROP VIEW hiredate_view ;  
GO  
CREATE VIEW hiredate_view  
AS   
SELECT p.FirstName, p.LastName, e.BusinessEntityID, e.HireDate  
FROM HumanResources.Employee e   
JOIN Person.Person AS p ON e.BusinessEntityID = p.BusinessEntityID ;  
GO  
select * from hiredate_view ;
