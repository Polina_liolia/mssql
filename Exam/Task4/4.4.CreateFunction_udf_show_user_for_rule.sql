USE [SampleDb];
GO

--������� ���������� ������ �������������, � ������� ���� �������� �����. ������ ������ :
--select dbo.udf_show_user_for_rule (�U�) as User_for_U;

IF OBJECT_ID (N'udf_show_user_for_rule', N'IF') IS NOT NULL  
    DROP FUNCTION [udf_show_user_for_rule];  
GO  
CREATE FUNCTION udf_show_user_for_rule(@rule nchar(30))
RETURNS TABLE AS
	RETURN 
	(
		SELECT 
		USERS.ID,
		USERS.USER_NAME
		FROM [dbo].[USERS_TO_ROLES] UTR
		LEFT JOIN [dbo].[ROLES] ROLES ON UTR.ROLE_ID = ROLES.ID
		LEFT JOIN [dbo].[USERS] USERS ON UTR.USER_ID = USERS.ID
		WHERE ROLES.[RULE] = @rule
	);

--TEST FUNCTION udf_show_user_for_rule
GO
USE [SampleDb];
select * FROM [dbo].[udf_show_user_for_rule] ('U') as User_for_U;
