USE [SampleDb]

--1. ������ ����������, �� ������� ���������� ����� ����� 12000 
SELECT
JOBS.JOB_TITLE,
JOBS.MIN_SALARY
FROM [HR].[jobs] AS JOBS
WHERE JOBS.MIN_SALARY < 12000
ORDER BY JOBS.MIN_SALARY ASC;

--2. ������ �������, � ������� �������� ����������, �� ����������, � ������� ����� 12000 
SELECT 
DEPTS.DEPARTMENT_NAME
FROM [HR].[Departments] AS DEPTS
LEFT JOIN [HR].[Employees] AS EMPL
ON EMPL.DEPARTMENT_ID = DEPTS.DEPARTMENT_ID
LEFT JOIN [HR].[jobs] AS JOBS
ON JOBS.JOB_ID = EMPL.JOB_ID
WHERE JOBS.MAX_SALARY > 12000 OR JOBS.MIN_SALARY > 12000
GROUP BY DEPTS.DEPARTMENT_NAME

--3. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � � ��������� ������, 
--� ������� �� �������� ������ 
--!this select is based on data about job and dept, taken from job_history table, but not from Employees table!
--that's why so many nulls in output
SELECT 
EMPL.LAST_NAME,
EMPL.FIRST_NAME,
JOBS.JOB_TITLE,
EMPL.salary,
DEPTS.DEPARTMENT_NAME,
JOBHIST.START_DATE
FROM [HR].[Employees] AS EMPL
LEFT JOIN [HR].[job_history] AS JOBHIST
ON JOBHIST.employee_id = EMPL.EMPLOYEE_ID
LEFT JOIN [HR].[Departments] AS DEPTS
ON DEPTS.DEPARTMENT_ID = JOBHIST.department_id
LEFT JOIN [HR].[jobs] AS JOBS
ON JOBS.JOB_ID = JOBHIST.job_id
WHERE JOBHIST.START_DATE IN
(
SELECT 
MAX (JOBHIST.start_date)
FROM [HR].[Employees] AS EMPL
LEFT JOIN [HR].[job_history] AS JOBHIST
ON JOBHIST.employee_id = EMPL.EMPLOYEE_ID
LEFT JOIN [HR].[Departments] AS DEPTS
ON DEPTS.DEPARTMENT_ID = JOBHIST.department_id
LEFT JOIN [HR].[jobs] AS JOBS
ON JOBS.JOB_ID = JOBHIST.job_id
GROUP BY EMPL.EMPLOYEE_ID
 ) OR JOBHIST.START_DATE IS NULL
ORDER BY EMPL.LAST_NAME

 
--4. �������� ������ �����������, ��� ���������� ������ ������������� �� 9
SELECT
EMPL.LAST_NAME,
EMPL.FIRST_NAME,
EMPL.PHONE_NUMBER
FROM [HR].[Employees] AS EMPL
WHERE RTRIM(EMPL.PHONE_NUMBER) LIKE '%9'


--5. �������� ������ �����������, ��� ���������� ������ ���������� � 590 � ������������� �� 0
SELECT *
/*EMPL.LAST_NAME,
EMPL.FIRST_NAME,
EMPL.PHONE_NUMBER*/
FROM [HR].[Employees] AS EMPL
WHERE LTRIM(RTRIM(EMPL.PHONE_NUMBER)) LIKE '590%0'

--6. �������� ������ ���������� � �������� �������, �������� ��� ��������� 
SELECT 
EMPL.LAST_NAME,
EMPL.FIRST_NAME,
DEPTS.DEPARTMENT_NAME
FROM [HR].[Departments] AS DEPTS
LEFT JOIN [HR].[Employees] AS EMPL
ON EMPL.EMPLOYEE_ID = DEPTS.MANAGER_ID
WHERE EMPL.EMPLOYEE_ID IS NOT NULL

--7. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � � ��������� ������,
-- � ������� �� �������� ������ ��������, � ����� ����������������� ���������� 
--!this select is based on data about job and dept, taken from job_history table, but not from Employees table!
--that's why so many nulls in output
SELECT 
EMPL.LAST_NAME,
EMPL.FIRST_NAME,
JOBS.JOB_TITLE,
EMPL.salary,
DEPTS.DEPARTMENT_NAME,
JOBHIST.START_DATE,
EMPL_MANAGERS.LAST_NAME AS 'MANAGERS LAST NAME'
FROM [HR].[Employees] AS EMPL
LEFT JOIN [HR].[job_history] AS JOBHIST
ON JOBHIST.employee_id = EMPL.EMPLOYEE_ID
LEFT JOIN [HR].[Departments] AS DEPTS
ON DEPTS.DEPARTMENT_ID = JOBHIST.department_id
LEFT JOIN [HR].[jobs] AS JOBS
ON JOBS.JOB_ID = JOBHIST.job_id
LEFT JOIN [HR].[Employees] AS EMPL_MANAGERS
ON EMPL.MANAGER_ID = EMPL_MANAGERS.EMPLOYEE_ID
WHERE JOBHIST.START_DATE IN
(
SELECT 
MAX (JOBHIST.start_date)
FROM [HR].[Employees] AS EMPL
LEFT JOIN [HR].[job_history] AS JOBHIST
ON JOBHIST.employee_id = EMPL.EMPLOYEE_ID
LEFT JOIN [HR].[Departments] AS DEPTS
ON DEPTS.DEPARTMENT_ID = JOBHIST.department_id
LEFT JOIN [HR].[jobs] AS JOBS
ON JOBS.JOB_ID = JOBHIST.job_id
GROUP BY EMPL.EMPLOYEE_ID
 ) OR JOBHIST.START_DATE IS NULL
ORDER BY EMPL.LAST_NAME


--8. ��������� ������� ���������� ����� ��������� 
SELECT
AVG(EMPL.salary) AS 'Average managers salary'
FROM [HR].[Employees] AS EMPL
WHERE EMPL.EMPLOYEE_ID IN
(
SELECT 
EMPL.MANAGER_ID
FROM [HR].[Employees] AS EMPL
WHERE EMPL.MANAGER_ID IS NOT NULL
UNION ALL
SELECT 
DEPTS.MANAGER_ID
FROM [HR].[Departments] AS DEPTS
WHERE DEPTS.MANAGER_ID IS NOT NULL
)


--9. �������� ������ ������� � ���������� ����������� � ������ �� ������� 
SELECT 
DEPTS.DEPARTMENT_ID,
DEPTS.DEPARTMENT_NAME,
COUNT (EMPL.EMPLOYEE_ID) AS 'Personnel count'
FROM [HR].[Departments] AS DEPTS
LEFT JOIN [HR].[Employees] AS EMPL
ON EMPL.DEPARTMENT_ID = DEPTS.DEPARTMENT_ID
GROUP BY DEPTS.DEPARTMENT_ID, DEPTS.DEPARTMENT_NAME


--10. �������� ������ ������� � ������� ���������� ����� � ��� 
SELECT 
DEPTS.DEPARTMENT_ID,
DEPTS.DEPARTMENT_NAME,
AVG (EMPL.SALARY) AS 'Average salary in department'
FROM [HR].[Departments] AS DEPTS
LEFT JOIN [HR].[Employees] AS EMPL
ON EMPL.DEPARTMENT_ID = DEPTS.DEPARTMENT_ID
GROUP BY DEPTS.DEPARTMENT_ID, DEPTS.DEPARTMENT_NAME


--11. �������� ������ ������� � ��������� ������ ������� �� ������ ����� ��� ����������� 
SELECT 
DEPTS.DEPARTMENT_ID,
DEPTS.DEPARTMENT_NAME,
SUM (EMPL.SALARY) AS 'SALARY BUDGET'
FROM [HR].[Departments] AS DEPTS
LEFT JOIN [HR].[Employees] AS EMPL
ON EMPL.DEPARTMENT_ID = DEPTS.DEPARTMENT_ID
GROUP BY DEPTS.DEPARTMENT_ID, DEPTS.DEPARTMENT_NAME

--12. ���������� ���� �� ������, � ������� �������� ����������, ��������������� ������������ ������
SELECT 
DEPTS.DEPARTMENT_ID,
DEPTS.DEPARTMENT_NAME
FROM [HR].[Departments] AS DEPTS
LEFT JOIN [HR].[Employees] AS EMPL
ON EMPL.DEPARTMENT_ID = DEPTS.DEPARTMENT_ID
WHERE EMPL.MANAGER_ID != DEPTS.MANAGER_ID AND EMPL.MANAGER_ID IS NOT NULL
GROUP BY DEPTS.DEPARTMENT_ID, DEPTS.DEPARTMENT_NAME