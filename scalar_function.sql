use [AdventureWorks2008];

go
IF OBJECT_ID (N'dbo.udf_Product', N'FN') IS NOT NULL
DROP FUNCTION dbo.udf_Product;
GO
CREATE FUNCTION dbo.udf_Product(@num1 INT, @num2 INT)
RETURNS INT AS
BEGIN
DECLARE @Product INT;
SET @Product = ISNULL(@num1,0) * ISNULL(@num2,0);
RETURN @Product;
END;

--test:
GO
select dbo.udf_Product( 5, 7) as Product;