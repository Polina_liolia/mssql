﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableParamTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security=True";
            //вызов ХП, с табличным параметром 
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                //in this example we create a data table with same name as the type we have in the DB 
                DataTable dataTable = new DataTable("SampleDataType"); //we create column names as per the type in DB 
                dataTable.Columns.Add("SampleString", typeof(string));
                dataTable.Columns.Add("SampleInt", typeof(Int32));
                //and fill in some values 
                dataTable.Rows.Add("99", 99);
                dataTable.Rows.Add("98", null);
                dataTable.Rows.Add("97", 99);
                SqlCommand command = sqlConnection.CreateCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "[dbo].[SampleProcedure]";
                SqlParameter parameter = new SqlParameter(); //The parameter for the SP must be of SqlDbType.Structured 
                parameter.ParameterName = "@Sample";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = dataTable;
                command.Parameters.Add(parameter);
                sqlConnection.Open();
                int numberOfRowsUpdated = command.ExecuteNonQuery(); //for SP, insert, update...
                Console.WriteLine("Number of rows updated = " + numberOfRowsUpdated);
            }
    }
    }
}
