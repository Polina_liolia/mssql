--use AdventureWorks2008
use [uh433027_db2]
select
[Login Type]=
case sp.type
when 'u' then 'WIN'
when 's' then 'SQL'
when 'g' then 'GRP'
end,
convert(char(45),sp.name) as srvLogin,
convert(char(45),sp2.name) as srvRole,
convert(char(25),dbp.name) as dbUser,
convert(char(25),dbp2.name) as dbRole
from
sys.server_principals as sp join
sys.database_principals as dbp on sp.sid=dbp.sid join
sys.database_role_members as dbrm on dbp.principal_Id=dbrm.member_principal_Id join
sys.database_principals as dbp2 on dbrm.role_principal_id=dbp2.principal_id left join
sys.server_role_members as srm on sp.principal_id=srm.member_principal_id left join
sys.server_principals as sp2 on srm.role_principal_id=sp2.principal_id


--- ���� ��� �������� ������������
go
WITH CTE_Roles (role_principal_id)
AS
(
SELECT role_principal_id
FROM sys.database_role_members
WHERE member_principal_id = USER_ID()
UNION ALL
SELECT drm.role_principal_id
FROM sys.database_role_members drm
  INNER JOIN CTE_Roles CR
    ON drm.member_principal_id = CR.role_principal_id
)
SELECT USER_NAME(role_principal_id) RoleName
FROM CTE_Roles
UNION ALL
SELECT 'public'
ORDER BY RoleName;


declare @user_name_db varchar(50), @password varchar(30), @sql_statement nvarchar(1000), @error_message nvarchar(1000)
 
set @user_name_db = 'a.s.pushkin2'
set @password = 'pamyatnik'
if not exists (select name from sys.server_principals  where type = 'S' and name = @user_name_db)
begin
 select @sql_statement = 'CREATE LOGIN "' + @user_name_db + '" WITH PASSWORD = ''' + @password+ ''', CHECK_POLICY = OFF'
 exec sp_executesql @sql_statement
end
if  (select  count(*) from sys.database_principals where name = @user_name_db) = 0
begin
 -- ������� ������������
 select @sql_statement = 'CREATE USER "' + @user_name_db + '" FOR LOGIN "' + @user_name_db+  '"'
 exec sp_executesql @sql_statement
 -- ����������� �����
 EXEC sp_addrolemember db_executor, @user_name_db
 EXEC sp_addrolemember db_datareader, @user_name_db
 EXEC sp_addrolemember db_datawriter, @user_name_db 
end
else
begin
 set  @error_message = N'������������  ' + @user_name_db + N' ��� ����������! '
 RAISERROR (@error_message, 14, 1)
end
