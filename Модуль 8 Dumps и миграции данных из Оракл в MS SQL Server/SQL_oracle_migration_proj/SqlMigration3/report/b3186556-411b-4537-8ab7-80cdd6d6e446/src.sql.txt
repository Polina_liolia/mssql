CREATE OR REPLACE TRIGGER hr.log_my_employees

before UPDATE or insert or delete on hr.my_emploees
for each row
declare
v_user_name varchar2(10);--пользователь соединения с бд
log_str varchar2(200);--текст протокола
begin
select user into v_user_name from dual;

log_str := 'EMPLOYEES_ID='||TO_CHAR(:old.EMPLOYEES_ID)||',EMPLOYEES_NAME='||TO_CHAR(:old.EMPLOYEES_NAME)||'-->'
||TO_CHAR(:new.EMPLOYEES_ID)||','||TO_CHAR(:new.EMPLOYEES_ID)||' by '||v_user_name;

insert into HR.MY_LOGS(ID,LOG_INFO) values (MY_LOGS_SEQUENCE.nextval,log_str);
end;
