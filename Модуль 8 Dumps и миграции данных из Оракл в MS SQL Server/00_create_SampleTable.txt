USE [AdventureWorks2008]
GO
SELECT OBJECT_ID(N'dbo.SampleTable') AS 'Object ID'
GO
IF OBJECT_ID (N'dbo.SampleTable', N'U') IS NOT NULL  
DROP TABLE [dbo].[SampleTable]
GO
--This is the database table
CREATE TABLE dbo.SampleTable
	(
	Id int NOT NULL IDENTITY (1, 1),
	SampleString nvarchar(64) NOT NULL,
	SampleInt int NULL
	)  ON [PRIMARY]
go