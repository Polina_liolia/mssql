use [AdventureWorks2008]
--if and while

--example 1

declare @k int;
set @k = 0;
while @k < 10
begin
	--print cast(@k as varchar(10))+N'-!';
	print cast(@k as varchar(10))+N'-�����';
	set @k = @k + 1;
end 


--example 2
--declare @k int;
--declare @N int;
--set @k = 0;
--set @N = 10;

--while @k < @N
--begin
--	if @k%2=0
--		begin
--		print cast(@k as varchar(10)) + N'-������';
--		end
--	else
--		begin
--		print cast(@k as varchar(10)) + N'-��������';
--		end
--	set @k=@k+1;
--end