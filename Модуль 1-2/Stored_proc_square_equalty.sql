IF OBJECT_ID ( 'CALC_SOLUTION_X1X2', 'P' ) IS NOT NULL 
    DROP PROCEDURE CALC_SOLUTION_X1X2;
GO
CREATE PROCEDURE CALC_SOLUTION_X1X2
@a INT,                       --Input parameter 1,  
@b INT,                       --Input parameter 2,  
@c INT,						  --Input parameter 2,  
@out_RESUN VARCHAR(200)  OUT, -- Out parameter declared with the help of OUT keyword
@x1 float  OUT,
@x2 float  OUT
as
SET NOCOUNT ON
DECLARE @d float;
set @d = SQRT(@b) - 4 * @a * @c;
IF @d > 0
begin
	select @x1= (-@b + SQRT(@d))/(2*@a); 
	select @x2= (-@b - SQRT(@d))/(2*@a); 
	set @out_RESUN = 'Yes';
end
else if @d = 0
begin
	select @x1= (-@b + SQRT(@d))/(2*@a); 
	set @x2= @x1;
	set @out_RESUN = 'Yes';
end
else set @out_RESUN = 'No';



