use [NORTHWIND.MDF]

--��������� ���� �������
ALTER TABLE [dbo].[Products] alter column [ProductName] nvarchar (100) not null

--���������� ������� 
ALTER TABLE [dbo].[Products] ADD TestProductName varchar(100) NULL, DateAddedTest datetime null;  

--�������� �������
ALTER TABLE [dbo].[Products] DROP COLUMN TestProductName, DateAddedTest;  

--�������� ��������� �������� ����������� ��� �������� �������� ������
SET IDENTITY_INSERT [dbo].[Categories] ON;
insert into [dbo].[Categories] ([CategoryID], [CategoryName]) values (-1, 'categoryTest');
SET IDENTITY_INSERT [dbo].[Categories] OFF;
SELECT *
FROM [dbo].[Categories]
