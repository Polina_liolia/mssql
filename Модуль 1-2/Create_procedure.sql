USE AdventureWorks2008
GO
IF OBJECT_ID ( 'HumanResources.uspGetAllEmployees', 'P' ) IS NOT NULL --'P' - �������� ���������
    DROP PROCEDURE HumanResources.uspGetAllEmployees;
GO
--����� �������� ��������� - ������ view (�� �� ���������������) 
CREATE PROCEDURE HumanResources.uspGetAllEmployees
AS
    SET NOCOUNT ON; --���� ���� �������� �� ����������, ��������� �������� ������� ����� ���������� ������������ �������
    SELECT LastName, FirstName, Department
    FROM HumanResources.vEmployeeDepartmentHistory;
GO


--���� ��� ������� ������ �������� ���������:

--1
EXECUTE HumanResources.uspGetAllEmployees;

--2
EXEC HumanResources.uspGetAllEmployees;

--3
GO
HumanResources.uspGetAllEmployees; --��� ����� ������� ������ ��, ������� �� ���������� ����������

--� ����� VIEW �������� ������ ���:
SELECT * FROM [dbo].[hiredate_view]

--SELECT * FROM HumanResources.uspGetAllEmployees --����� ����� ��� �� ��������, ���� �� �� ���������� �������
