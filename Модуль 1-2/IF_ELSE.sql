--������������� ��������� ���������� IF-ELSE
DECLARE @Number int;
SET @Number = 50;
IF @Number > 100
���PRINT 'The number is large.';
ELSE 
���BEGIN
������IF @Number < 10
������PRINT 'The number is small.';
���ELSE
������PRINT 'The number is medium.';
���END ;

use [AdventureWorks2008]

--� ��������� ������� ������ ����������� ��� ����� ����������� ���������, � ����� ����������� ������� ������������
--����� ����������, ���������� �� ���������� ����������� ���������. ������ ���� ���������� ���������� �BEGIN�� ������������END.
DECLARE @AvgWeight decimal(8,2), @BikeCount int
IF 
(SELECT COUNT(*) FROM Production.Product WHERE Name LIKE 'Touring-3000%' ) > 5
BEGIN
���SET @BikeCount = 
��������(SELECT COUNT(*) 
���������FROM Production.Product 
���������WHERE Name LIKE 'Touring-3000%');
���SET @AvgWeight = 
��������(SELECT AVG(Weight) 
���������FROM Production.Product 
���������WHERE Name LIKE 'Touring-3000%');
���PRINT 'There are ' + CAST(@BikeCount AS varchar(3))�+ ' Touring-3000 bikes.'
���PRINT 'The average weight of the top 5 Touring-3000 bikes is ' + CAST(@AvgWeight AS varchar(8)) + '.';
END
ELSE 
BEGIN
SET @AvgWeight = 
��������(SELECT AVG(Weight)
���������FROM Production.Product 
���������WHERE Name LIKE 'Touring-3000%' );
���PRINT 'Average weight of the Touring-3000 bikes is ' + CAST(@AvgWeight AS varchar(8)) + '.' ;
END ;


--������������� �������� ���� BREAK � CONTINUE ������ ��������� ����������� IF...ELSE � WHILE

WHILE (SELECT AVG(ListPrice) FROM Production.Product) < $300
BEGIN
   UPDATE Production.Product
      SET ListPrice = ListPrice * 2
   SELECT MAX(ListPrice) FROM Production.Product
   IF (SELECT MAX(ListPrice) FROM Production.Product) > $500
      BREAK
   ELSE
      CONTINUE
END
PRINT 'Too much for the market to bear';