USE AdventureWorks2008;

--�������� ��������� ���������� - ���������� ���������� ���� table
GO
DECLARE @MyTableVar table(
    EmpID int NOT NULL,
    OldVacationHours int,
    NewVacationHours int,
    ModifiedDate datetime);
UPDATE TOP (10) HumanResources.Employee
SET VacationHours = VacationHours * 1.25,
    ModifiedDate = GETDATE() 
OUTPUT inserted.BusinessEntityID,
       deleted.VacationHours,
       inserted.VacationHours,
       inserted.ModifiedDate
INTO @MyTableVar;
--Display the result set of the table variable.
SELECT EmpID, OldVacationHours, NewVacationHours, ModifiedDate
FROM @MyTableVar;
GO
--Display the result set of the table.
SELECT TOP (10) BusinessEntityID, VacationHours, ModifiedDate
FROM HumanResources.Employee;
GO



--��������� ������ � ������� �������� ���������: ������������� UPDATE � �������� ���������
GO
IF OBJECT_ID ('HumanResources.Update_VacationHours', 'P') IS NOT NULL
DROP PROCEDURE HumanResources.Update_VacationHours;
GO
CREATE PROCEDURE HumanResources.Update_VacationHours
@NewHours smallint
AS 
SET NOCOUNT ON;
UPDATE HumanResources.Employee
SET VacationHours = 
    ( CASE
         WHEN SalariedFlag = 0 THEN VacationHours + @NewHours
         ELSE @NewHours
       END
    )
WHERE CurrentFlag = 1;
GO

EXEC HumanResources.Update_VacationHours 40;

--test
SELECT *
FROM 
HumanResources.Employee

--������������� ������������ ��������� �������� ����������
/* Create a table type. */
CREATE TYPE LocationTableType AS TABLE 
( LocationName VARCHAR(50)
, CostRate INT );
GO

/* Create a procedure to receive data for the table-valued parameter. */
CREATE PROCEDURE usp_InsertProductionLocation
    @TVP LocationTableType READONLY
    AS 
    SET NOCOUNT ON
    INSERT INTO [AdventureWorks2008].[Production].[Location]
           ([Name]
           ,[CostRate]
           ,[Availability]
           ,[ModifiedDate])
        SELECT *, 0, GETDATE()
        FROM  @TVP;
GO

/* Declare a variable that references the type. */
DECLARE @LocationTVP 
AS LocationTableType;

/* Add data to the table variable. */
INSERT INTO @LocationTVP (LocationName, CostRate)
    SELECT [Name], 0.00
    FROM 
    [AdventureWorks2008].[Person].[StateProvince];

/* Pass the table variable data to a stored procedure. */
EXEC usp_InsertProductionLocation @LocationTVP;
GO


--�������, ������������ ������������� (UDF)
USE AdventureWorks2008;
go
IF OBJECT_ID (N'dbo.udf_Product', N'FN') IS NOT NULL  
    DROP FUNCTION dbo.udf_Product;  
GO  
CREATE FUNCTION dbo.udf_Product(@num1 INT, @num2 INT)
RETURNS INT AS
BEGIN
	DECLARE @Product INT;
	SET @Product = ISNULL(@num1,0) * ISNULL(@num2,0);
	RETURN @Product;
END;
GO
select dbo.udf_Product( 2, 7) as Product;