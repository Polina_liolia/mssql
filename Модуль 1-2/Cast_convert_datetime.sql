-- 6 ������������� ������� CAST � CONVERT � ������� ���� datetime
--��������� ������ ���������� ������� ���� � �����, ���������� ������� CAST ��� ��������� 
--������� ���� � ������� � ���������� ��� ������ � ����� ���������� CONVERT ��� ����������� ���� � ������� � ������� ISO 8901.

SELECT 
   GETDATE() AS UnconvertedDateTime,
   CAST(GETDATE() AS nvarchar(30)) AS UsingCast,
   CONVERT(nvarchar(30), GETDATE(), 126) AS UsingConvertTo_ISO8601  

--������ 6
--��������� ������ � ��������� ����������������� ����������� �������. ������ ���������� ���� � ����� � ���� ���������� ������,
--���������� ������� CAST ��� ��������� ���������� ������ � ��� ������ datetime, � ����� ���������� ������� CONVERT ��� ���������
--���������� ������ � ��� ������ datetime.
SELECT 
   '2006-04-25T15:50:59.997' AS UnconvertedText,
   CAST('2006-04-25T15:50:59.997' AS datetime) AS UsingCast,
   CONVERT(datetime, '2006-04-25T15:50:59.997', 126) AS UsingConvertFrom_ISO8601 ;
