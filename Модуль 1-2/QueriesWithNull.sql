declare @s1 int;
declare @s2 int;
declare @s3 int;
declare @s4 int;
declare @s5 int;

set @s1 = 100;
set @s2 = 200;
set @s3 = NULL;
set @s4 = 50;
set @s5 = 50;


select (@s1 + @s2 + @s3 + @s4 + @s5) as Sum_salary;	--NULL, �.�. ���� �� ���� �������� �� ��������� NULL �������� � ��������������� ����������
select ('Salary = ' + CAST(@s1 as varchar(100))) as Salary1;
select ('Salary = ' + CAST(@s2 as varchar(100))) as Salary2;
select ('Salary = ' + CAST(@s3 as varchar(100))) as Salary3; --NULL, �.�. ����� �������� � NULL �������� � ��������������� ����������
select ('Salary = ' + CAST(@s4 as varchar(100))) as Salary4;
select ('Salary = ' + CAST(@s5 as varchar(100))) as Salary5;

use [AdventureWorks2008]
--manual changing null values:
select
ProductNumber,
ProductLine, --original value
Category = --changed value
	CASE ProductLine
		when 'R' then 'Road'
		when 'M' then 'Mountain'
		when 'T' then 'Touring'
		when 'S' then 'Other sale items'
		else 'Not for sale'
	END,
Name
from Production.Product
order by ProductNumber


--changing null values using coalesce function:
select
ProductNumber,
ProductLine,
coalesce(ProductLine, 'not for sale') as ProductLineNoNulls,
Name
from Production.Product
order by ProductNumber

--manual setting null if true
select 
ProductID, 
MakeFlag,
FinishedGoodsFlag,
'Null if equal' = 
	case
		when MakeFlag = FinishedGoodsFlag then null
		else MakeFlag
	end
from Production.Product
where ProductID < 10;

--using nullif to set null if true
select 
ProductID, 
MakeFlag,
FinishedGoodsFlag,
nullif(MakeFlag, FinishedGoodsFlag) as 'Null if equal'
from Production.Product
where ProductID < 10;

--������ 3 ������������� ������� ISNULL, ������� �������� �������� NULL ��������� ���������� ���������. ���������: ISNULL ( check_expression , replacement_value )
SELECT AVG(ISNULL(Weight, 50))
FROM Production.Product;
SELECT Description, DiscountPct, MinQty, ISNULL(MaxQty, 0.00) AS 'Max Quantity'
FROM Sales.SpecialOffer;

--������ 4 ������������� COALSCE, ������� ���������� ������ ��������� �� ������ ����������, �� ������ NULL.
SELECT Name, Class, Color, ProductNumber,
COALESCE(Class, Color, ProductNumber) AS FirstNotNull
FROM Production.Product;


