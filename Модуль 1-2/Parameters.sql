use [NORTHWIND.MDF]

--�� ����������� �������:
select *
from [dbo].[Categories]
where [CategoryID] = 2; --����� �������������� ������

declare @Parametr int --�������� ���������� � Transact-SQL
set @Parametr = 5; --��������� ��������; ����� ����� ���� �������� ��������� �������

--������ � �������������� ����������:
select *
from [dbo].[Categories]
where [CategoryID] = @Parametr; --����� �������������� ������

--������ �������������� �����:
declare @A varchar(2)
declare @B varchar(2)
declare @C varchar(2)
declare @D int

set @A = 25;
set @B = 15;
set @C = 35;
set @D = (select cast(@A as int) + cast(@B as int) + cast(@C as int));

select @D as Result;

