USE AdventureWorks2008;
GO
IF OBJECT_ID ( 'HumanResources.uspEncryptThis', 'P' ) IS NOT NULL 
    DROP PROCEDURE HumanResources.uspEncryptThis;
GO
CREATE PROCEDURE HumanResources.uspEncryptThis
--WITH ENCRYPTION
AS
    SET NOCOUNT ON;
    SELECT BusinessEntityID, JobTitle, NationalIDNumber, VacationHours, SickLeaveHours 
    FROM HumanResources.Employee;
GO

EXEC sp_helptext 'HumanResources.uspEncryptThis'; --���� �� �� �����������, ������� � ����� (��� ������ �����). ����� - ������ �� �������

SELECT definition FROM sys.sql_modules --���� �� �� �����������, ������� � ����� (��� ���� ������). ����� - ������ null
WHERE object_id = OBJECT_ID('HumanResources.uspEncryptThis');