Declare @res varchar(200)   -- Declaring the variable to collect the discriminant
Declare @x1 float    -- Declaring the variable to collect the x1
Declare @x2 float    -- Declaring the variable to collect the x2
set @res = 'NOT def';
Execute [dbo].CALC_SOLUTION_X1X2 1, 0, -9, @res output, @x1 output, @x2 output
select @res as d,@x1 as x1,@x2 as x2 -- "Select" Statement is used to show the output from Procedure
