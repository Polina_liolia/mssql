USE AdventureWorks2008 
GO  
IF OBJECT_ID ('Purchasing.PurchaseOrderReject', 'V') IS NOT NULL  
    DROP VIEW Purchasing.PurchaseOrderReject ;  
GO  
CREATE VIEW Purchasing.PurchaseOrderReject  
WITH ENCRYPTION  --������������� view ����� ������������ ��� ����������, �� ��� ��� ����������� �� �������
AS  
SELECT PurchaseOrderID, ReceivedQty, RejectedQty,   
    RejectedQty / ReceivedQty AS RejectRatio, DueDate  
FROM Purchasing.PurchaseOrderDetail  
WHERE RejectedQty / ReceivedQty > 0  
AND DueDate > CONVERT(DATETIME,'20010630',101) ;  
GO 
select * from Purchasing.PurchaseOrderReject 

go
ALTER VIEW [Purchasing].[PurchaseOrderReject]
with encryption --���� ������������ ���� ��������, ���������� ������ ��� �� ����� ����������. ����� - ���������� �� �����
as
SELECT PurchaseOrderID,RejectedQty,   
    RejectedQty / ReceivedQty AS RejectRatio, DueDate  
FROM Purchasing.PurchaseOrderDetail  
WHERE RejectedQty / ReceivedQty > 0  
AND DueDate > CONVERT(DATETIME,'20010630',101) ;  
GO 
select * from Purchasing.PurchaseOrderReject 

